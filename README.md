This project is an experiment, and the goal is to try to load Kotlin scripts (.kts) inside a Spring Boot app at runtime
to execute their contents.

The app is a simple REST web app, that can take puzzle inputs from
the [Advent of Code website](https://adventofcode.com/)
in the requests and give back the puzzle answer in the responses. Each script will contain the code to solve a given
problem.

For the first steps, the scripts are packaged directly inside the Docker image inside the aoc-scripts folder but if the
proof of concept is conclusive, they could be loaded from an external database for example.

TODOs:
======

- [x] Simple Gitlab CI pipeline to create the Docker image with the Spring Boot app and the external scripts, then
  deploy it on Google Cloud Run.

- [x] Read two .kts puzzle solver scripts at runtime and create the endpoints to execute their contents.

- [ ] Complete with a few scripts to solve more puzzles.

- [ ] Check the possible performance impacts of executing Kotlin scripts inside a JVM.

- [ ] Create authenticated endpoints to manage scripts (upload/read/delete) from Google Firestore.

Available dev endpoints:
========================

```bash
# List available puzzles to solve
curl --location --request GET 'https://advent-of-code-puzzle-solver-lobbr6byoq-uc.a.run.app/puzzles'

# Display info about an individual puzzle by id
curl --location --request GET 'https://advent-of-code-puzzle-solver-lobbr6byoq-uc.a.run.app/puzzles/2019-day01-part1'

# Display the Kotlin script used to solve the puzzle 
curl --location --request GET 'https://advent-of-code-puzzle-solver-lobbr6byoq-uc.a.run.app/puzzles/2019-day01-part1/solver-script'

# Create a solution to a puzzle by passing the puzzle input in the request body
# TODO

```

Useful commands for local development:
======================================

```bash
# Make Intellij Idea formatter produce ktlint compatible files
./gradlew ktlintapplyToIdea

# Build image to local Docker daemon
./gradlew jibDockerBuild

# Open a shell inside a running container (useful to check the presence of aoc-scripts)
docker run -it --entrypoint sh us-east1-docker.pkg.dev/aoc-puzzle-solver/aoc-puzzle-solver-repo/backend:latest

# Run the application
docker run -p 8080:8080 us-east1-docker.pkg.dev/aoc-puzzle-solver/aoc-puzzle-solver-repo/backend:latest

```

Notes:
======

- Investigate issues between Kotlin 1.4.32 and 1.5.20 for mockk usages

- https://dzone.com/articles/compiling-kotlin-in-runtime