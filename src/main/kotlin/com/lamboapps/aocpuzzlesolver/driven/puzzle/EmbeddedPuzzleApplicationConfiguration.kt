package com.lamboapps.aocpuzzlesolver.driven.puzzle

import com.lamboapps.aocpuzzlesolver.domain.PuzzleRepository
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(EmbeddedPuzzleApplicationProperties::class)
class EmbeddedPuzzleApplicationConfiguration {

    @Bean
    fun puzzleRepository(properties: EmbeddedPuzzleApplicationProperties): PuzzleRepository =
        EmbeddedPuzzleRepository(properties)
}
