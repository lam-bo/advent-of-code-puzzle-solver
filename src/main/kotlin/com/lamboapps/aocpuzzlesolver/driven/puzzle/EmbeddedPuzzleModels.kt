package com.lamboapps.aocpuzzlesolver.driven.puzzle

import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleInputFormat

enum class EmbeddedPuzzleInputFormat {
    MULTI_LINE, COMMA_SEPARATED;

    fun toDomain(): PuzzleInputFormat = PuzzleInputFormat.valueOf(this.name)
}

data class EmbeddedPuzzle(
    val id: String,
    val url: String,
    val inputFormat: EmbeddedPuzzleInputFormat,
    val solverPath: String
) {

    fun toDomain(): Puzzle = Puzzle(id, url, inputFormat.toDomain())
}
