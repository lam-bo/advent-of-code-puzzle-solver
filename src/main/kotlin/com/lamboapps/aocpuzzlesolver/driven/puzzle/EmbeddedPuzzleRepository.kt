package com.lamboapps.aocpuzzlesolver.driven.puzzle

import arrow.core.Either
import arrow.core.left
import arrow.core.leftIfNull
import arrow.core.right
import com.lamboapps.aocpuzzlesolver.domain.AppError
import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.domain.NotFoundError
import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleRepository
import java.io.File

class EmbeddedPuzzleRepository(
    private val properties: EmbeddedPuzzleApplicationProperties,
    private val scriptsCache: MutableMap<String, String> = mutableMapOf()
) : PuzzleRepository {
    companion object {
        const val NO_PUZZLE_FOUND_MESSAGE: String = "No puzzle was found"
        const val SOLVER_SCRIPT_RETRIEVAL_ERROR: String = "An error occurred during script retrieval"
    }

    override suspend fun getPuzzles(): Either<AppError, Set<Puzzle>> {
        return properties.embeddedPuzzles.distinctBy { it.id }.map { it.toDomain() }.toSet().right()
    }

    override suspend fun getPuzzle(id: String): Either<AppError, Puzzle> {
        return getPuzzles().map { puzzles -> puzzles.find { it.id == id } }
            .leftIfNull { NotFoundError(NO_PUZZLE_FOUND_MESSAGE) }
    }

    override suspend fun getSolverScript(puzzle: Puzzle): Either<AppError, String> {
        return runCatching {
            scriptsCache.computeIfAbsent(puzzle.id) { puzzleId ->
                val embeddedPuzzle = properties.embeddedPuzzles.find { it.id == puzzleId }
                val embeddedPuzzleScript = embeddedPuzzle?.let { File(it.solverPath).readText() }
                embeddedPuzzleScript ?: throw NullPointerException(NO_PUZZLE_FOUND_MESSAGE)
            }
        }.fold({ it.right() }, { InternalAppError(SOLVER_SCRIPT_RETRIEVAL_ERROR, it).left() })
    }
}
