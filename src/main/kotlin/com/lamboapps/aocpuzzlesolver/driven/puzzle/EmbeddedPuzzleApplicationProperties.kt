package com.lamboapps.aocpuzzlesolver.driven.puzzle

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("com.lamboapps.aocpuzzlesolver.driven.puzzle")
data class EmbeddedPuzzleApplicationProperties(val embeddedPuzzles: List<EmbeddedPuzzle>)
