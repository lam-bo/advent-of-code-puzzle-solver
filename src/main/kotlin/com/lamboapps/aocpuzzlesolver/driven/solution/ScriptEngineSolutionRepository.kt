package com.lamboapps.aocpuzzlesolver.driven.solution

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.lamboapps.aocpuzzlesolver.domain.AppError
import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.domain.SolutionRepository
import org.jetbrains.kotlin.cli.common.environment.setIdeaIoUseFallback
import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngine
import javax.script.CompiledScript
import javax.script.ScriptEngineManager

class ScriptEngineSolutionRepository(
    private val compiledScriptsCache: MutableMap<Int, CompiledScript> = mutableMapOf()
) : SolutionRepository {
    companion object {
        const val SOLVER_SCRIPT_EVALUATION_ERROR: String = "An error occurred during script evaluation"
    }

    private val scriptEngine = run {
        setIdeaIoUseFallback()
        val factory = ScriptEngineManager().getEngineByExtension("kts").factory
        factory.scriptEngine as KotlinJsr223JvmLocalScriptEngine
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun createSolution(puzzleInput: String, solverScript: String): Either<AppError, String> {
        return runCatching {
            val cacheKey = solverScript.hashCode()
            val compiledScript = compiledScriptsCache.computeIfAbsent(cacheKey) { scriptEngine.compile(solverScript) }
            val solutionFunction = compiledScript.eval() as ((String) -> String)
            solutionFunction.invoke(puzzleInput)
        }.fold({ it.right() }, { InternalAppError(SOLVER_SCRIPT_EVALUATION_ERROR, it).left() })
    }
}
