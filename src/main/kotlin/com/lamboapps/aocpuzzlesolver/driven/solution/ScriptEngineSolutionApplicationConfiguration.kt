package com.lamboapps.aocpuzzlesolver.driven.solution

import com.lamboapps.aocpuzzlesolver.domain.SolutionRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ScriptEngineSolutionApplicationConfiguration {

    @Bean
    fun solutionRepository(): SolutionRepository = ScriptEngineSolutionRepository()
}
