package com.lamboapps.aocpuzzlesolver.domain

interface AppError {
    val message: String
    val throwable: Throwable?
    val details: List<String>
}

data class ValidationError(
    override val message: String,
    override val throwable: Throwable? = null,
    override val details: List<String> = emptyList()
) : AppError

data class NotFoundError(
    override val message: String,
    override val throwable: Throwable? = null,
    override val details: List<String> = emptyList()
) : AppError

data class NotImplementedError(
    override val message: String,
    override val throwable: Throwable? = null,
    override val details: List<String> = emptyList()
) : AppError

data class InternalAppError(
    override val message: String,
    override val throwable: Throwable? = null,
    override val details: List<String> = emptyList()
) : AppError
