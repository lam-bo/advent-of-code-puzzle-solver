package com.lamboapps.aocpuzzlesolver.domain

import arrow.core.Either

interface SolutionRepository {
    suspend fun createSolution(puzzleInput: String, solverScript: String): Either<AppError, String>
}
