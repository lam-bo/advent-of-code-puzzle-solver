package com.lamboapps.aocpuzzlesolver.domain

data class Puzzle(val id: String, val url: String, val inputFormat: PuzzleInputFormat)

enum class PuzzleInputFormat {
    MULTI_LINE, COMMA_SEPARATED
}
