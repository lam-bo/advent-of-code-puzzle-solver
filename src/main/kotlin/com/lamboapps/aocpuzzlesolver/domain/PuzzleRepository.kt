package com.lamboapps.aocpuzzlesolver.domain

import arrow.core.Either

interface PuzzleRepository {

    suspend fun getPuzzles(): Either<AppError, Set<Puzzle>>

    suspend fun getPuzzle(id: String): Either<AppError, Puzzle>

    suspend fun getSolverScript(puzzle: Puzzle): Either<AppError, String>
}
