package com.lamboapps.aocpuzzlesolver.driving.api.puzzle

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.lamboapps.aocpuzzlesolver.domain.AppError
import com.lamboapps.aocpuzzlesolver.domain.PuzzleRepository
import com.lamboapps.aocpuzzlesolver.domain.SolutionRepository
import com.lamboapps.aocpuzzlesolver.driving.api.common.CorrelationIdHeaderFilter.Companion.CORRELATION_ID_HEADER_NAME
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationContext
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationRecorder
import com.lamboapps.aocpuzzlesolver.driving.api.common.ResponseEntityAdapter
import com.lamboapps.aocpuzzlesolver.driving.api.puzzle.PuzzleValidation.validatePuzzleInput
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController

@RestController
class PuzzleController(
    val puzzleRepository: PuzzleRepository,
    val solutionRepository: SolutionRepository,
    val operationRecorder: OperationRecorder
) {
    companion object {
        const val PUZZLES_GET_OPERATION_ID: String = "getPuzzles"
        const val PUZZLE_GET_OPERATION_ID: String = "getPuzzle"
        const val PUZZLE_SOLVER_SCRIPT_GET_OPERATION_ID: String = "getPuzzleSolverScript"
        const val PUZZLE_SOLUTION_POST_OPERATION_ID: String = "postSolution"
        const val PUZZLE_ID_KEY = "puzzleId"
    }

    @GetMapping("/puzzles", produces = [MediaType.APPLICATION_JSON_VALUE])
    suspend fun getPuzzles(@RequestHeader(CORRELATION_ID_HEADER_NAME) correlationId: String): ResponseEntity<Any> {
        return puzzleRepository.getPuzzles()
            .map { puzzles -> puzzles.map { PuzzleModel.fromDomain(it) } }
            .also { recordResult(PUZZLES_GET_OPERATION_ID, it, correlationId) }
            .fold(
                { appError -> ResponseEntityAdapter.fromAppError(appError) },
                { apiModel -> ResponseEntityAdapter.fromResponse(HttpStatus.OK, apiModel) }
            )
    }

    @GetMapping("/puzzles/{$PUZZLE_ID_KEY}")
    suspend fun getPuzzle(
        @RequestHeader(CORRELATION_ID_HEADER_NAME) correlationId: String,
        @PathVariable(PUZZLE_ID_KEY) puzzleId: String
    ): ResponseEntity<Any> {
        return puzzleRepository.getPuzzle(puzzleId)
            .map { puzzle -> PuzzleModel.fromDomain(puzzle) }
            .also { recordResult(PUZZLE_GET_OPERATION_ID, it, correlationId, puzzleId) }
            .fold(
                { appError -> ResponseEntityAdapter.fromAppError(appError) },
                { apiModel -> ResponseEntityAdapter.fromResponse(HttpStatus.OK, apiModel) }
            )
    }

    @GetMapping("/puzzles/{$PUZZLE_ID_KEY}/solver-script")
    suspend fun getPuzzleSolverScript(
        @RequestHeader(CORRELATION_ID_HEADER_NAME) correlationId: String,
        @PathVariable(PUZZLE_ID_KEY) puzzleId: String
    ): ResponseEntity<Any> {
        return puzzleRepository.getPuzzle(puzzleId)
            .flatMap { puzzle -> puzzleRepository.getSolverScript(puzzle) }
            .also { recordResult(PUZZLE_SOLVER_SCRIPT_GET_OPERATION_ID, it, correlationId, puzzleId) }
            .fold(
                { appError -> ResponseEntityAdapter.fromAppError(appError) },
                { plainText -> ResponseEntityAdapter.fromResponse(HttpStatus.OK, plainText, MediaType.TEXT_PLAIN) }
            )
    }

    @PostMapping("/puzzles/{$PUZZLE_ID_KEY}/solutions", consumes = [MediaType.TEXT_PLAIN_VALUE])
    suspend fun createPuzzleSolution(
        @RequestHeader(CORRELATION_ID_HEADER_NAME) correlationId: String,
        @PathVariable(PUZZLE_ID_KEY) puzzleId: String,
        @RequestBody puzzleInput: String
    ): ResponseEntity<Any> {
        return puzzleRepository.getPuzzle(puzzleId)
            .flatMap { puzzle -> validatePuzzleInput(puzzleInput, puzzle)?.left() ?: puzzle.right() }
            .flatMap { puzzle -> puzzleRepository.getSolverScript(puzzle) }
            .flatMap { solverScript -> solutionRepository.createSolution(puzzleInput, solverScript) }
            .also { recordResult(PUZZLE_SOLUTION_POST_OPERATION_ID, it, correlationId, puzzleId) }
            .fold(
                { appError -> ResponseEntityAdapter.fromAppError(appError) },
                { plainText -> ResponseEntityAdapter.fromResponse(HttpStatus.OK, plainText, MediaType.TEXT_PLAIN) }
            )
    }

    private fun recordResult(
        operationId: String,
        result: Either<AppError, Any>,
        correlationId: String,
        puzzleId: String? = null
    ) {
        val context = OperationContext.Builder()
            .putNullable(PUZZLE_ID_KEY, puzzleId)
            .putAppError(result.swap().orNull())
            .build(operationId, correlationId)
        when (result) {
            is Either.Left -> operationRecorder.recordOperationFailure(context, result.value)
            is Either.Right -> operationRecorder.recordOperationSuccess(context)
        }
    }
}
