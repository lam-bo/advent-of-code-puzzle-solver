package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.lamboapps.aocpuzzlesolver.domain.AppError
import org.slf4j.Logger

// TODO use GCP structured logs format, send metrics
class OperationRecorder(private val logger: Logger) {

    private val objectMapper: ObjectMapper = ObjectMapper()

    fun recordOperationSuccess(context: OperationContext) {
        logger.info(objectMapper.writeValueAsString(context.logEntries))
    }

    fun recordOperationFailure(context: OperationContext, appError: AppError) {
        logger.error(objectMapper.writeValueAsString(context.logEntries), appError.throwable)
    }
}
