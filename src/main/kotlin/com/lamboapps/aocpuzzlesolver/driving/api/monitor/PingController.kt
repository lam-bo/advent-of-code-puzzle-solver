package com.lamboapps.aocpuzzlesolver.driving.api.monitor

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class PingController {

    @GetMapping("/ping")
    suspend fun pong(): String = "PONG"
}
