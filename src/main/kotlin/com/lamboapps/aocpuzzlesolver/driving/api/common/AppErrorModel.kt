package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.domain.AppError

data class AppErrorModel(val message: String, val details: List<String>) {

    companion object {
        fun fromDomain(appError: AppError): AppErrorModel = AppErrorModel(appError.message, appError.details)
    }
}
