package com.lamboapps.aocpuzzlesolver.driving.api.puzzle

import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleInputFormat

data class PuzzleModel(val id: String, val url: String, val inputFormat: PuzzleInputFormatModel) {

    companion object {
        fun fromDomain(puzzle: Puzzle): PuzzleModel =
            PuzzleModel(puzzle.id, puzzle.url, PuzzleInputFormatModel.fromDomain(puzzle.inputFormat))
    }
}

enum class PuzzleInputFormatModel {
    MULTI_LINE, COMMA_SEPARATED;

    companion object {
        fun fromDomain(inputFormat: PuzzleInputFormat): PuzzleInputFormatModel = valueOf(inputFormat.name)
    }
}
