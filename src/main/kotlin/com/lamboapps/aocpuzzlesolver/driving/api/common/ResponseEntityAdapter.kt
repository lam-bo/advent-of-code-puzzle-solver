package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.domain.AppError
import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.domain.NotFoundError
import com.lamboapps.aocpuzzlesolver.domain.NotImplementedError
import com.lamboapps.aocpuzzlesolver.domain.ValidationError
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity

object ResponseEntityAdapter {
    fun fromAppError(appError: AppError): ResponseEntity<Any> {
        val httpStatus = when (appError) {
            is NotFoundError -> HttpStatus.NOT_FOUND
            is NotImplementedError -> HttpStatus.NOT_IMPLEMENTED
            is ValidationError -> HttpStatus.BAD_REQUEST
            is InternalAppError -> HttpStatus.INTERNAL_SERVER_ERROR
            else -> HttpStatus.INTERNAL_SERVER_ERROR
        }
        return ResponseEntity.status(httpStatus)
            .contentType(MediaType.APPLICATION_JSON)
            .body(AppErrorModel.fromDomain(appError))
    }

    fun fromResponse(
        status: HttpStatus,
        body: Any? = null,
        mediaType: MediaType = MediaType.APPLICATION_JSON
    ): ResponseEntity<Any> {
        return ResponseEntity.status(status)
            .apply { body?.let { this.contentType(mediaType) } }
            .body(body)
    }
}
