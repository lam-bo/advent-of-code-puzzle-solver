package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.domain.AppError

data class OperationContext(val logEntries: Map<String, String>) {

    class Builder {
        companion object {
            const val APP_ERROR_MESSAGE_KEY = "errorMessage"
            const val APP_ERROR_DETAILS_KEY = "errorDetails"
            const val CORRELATION_ID_KEY = "correlationId"
            const val OPERATION_ID_KEY = "operationId"
        }

        private val logEntriesBuilder: MutableMap<String, String> = mutableMapOf()

        fun put(key: String, value: String) = apply { this.logEntriesBuilder[key] = value }
        fun putNullable(key: String, value: String?) = apply { value?.let { this.logEntriesBuilder[key] = it } }
        fun putTry(key: String, value: Result<String>) =
            apply { value.getOrNull()?.let { this.logEntriesBuilder[key] = it } }

        fun putAppError(error: AppError?) = apply {
            error?.let {
                this.logEntriesBuilder[APP_ERROR_MESSAGE_KEY] = it.message
                this.logEntriesBuilder[APP_ERROR_DETAILS_KEY] = it.details.joinToString("\n")
            }
        }

        fun build(operationId: String, correlationId: String): OperationContext {
            this.logEntriesBuilder[OPERATION_ID_KEY] = operationId
            this.logEntriesBuilder[CORRELATION_ID_KEY] = correlationId
            return OperationContext(this.logEntriesBuilder)
        }
    }
}
