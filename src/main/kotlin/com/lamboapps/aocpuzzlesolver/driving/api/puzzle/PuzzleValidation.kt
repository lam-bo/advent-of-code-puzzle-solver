package com.lamboapps.aocpuzzlesolver.driving.api.puzzle

import com.lamboapps.aocpuzzlesolver.domain.AppError
import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleInputFormat
import com.lamboapps.aocpuzzlesolver.domain.ValidationError

object PuzzleValidation {
    const val PUZZLE_INPUT_FORMAT_VALIDATION_ERROR = "The puzzle input is not correctly formatted"

    fun validatePuzzleInput(puzzleInput: String, puzzle: Puzzle): AppError? {
        val predicate: (String) -> Boolean = when (puzzle.inputFormat) {
            PuzzleInputFormat.COMMA_SEPARATED -> { input -> input.lines().size == 1 && input.split(',').size > 1 }
            PuzzleInputFormat.MULTI_LINE -> { input -> input.lines().size > 1 }
        }
        return when (predicate.invoke(puzzleInput)) {
            true -> null
            false -> ValidationError(PUZZLE_INPUT_FORMAT_VALIDATION_ERROR)
        }
    }
}
