package com.lamboapps.aocpuzzlesolver.driving.api.common

import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

class CorrelationIdHeaderFilter(private val correlationIdProvider: () -> String) : WebFilter {
    companion object {
        const val CORRELATION_ID_HEADER_NAME: String = "X-Correlation-ID"
    }

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val correlationId = getRequestCorrelationId(exchange) ?: correlationIdProvider.invoke()
        val exchangeWithCorrelationId = exchange.takeIf { getRequestCorrelationId(it) != null } ?: run {
            val mutatedRequest = exchange.request.mutate().header(CORRELATION_ID_HEADER_NAME, correlationId).build()
            exchange.mutate().request(mutatedRequest).build()
        }
        exchangeWithCorrelationId.response.headers.set(CORRELATION_ID_HEADER_NAME, correlationId)
        return chain.filter(exchangeWithCorrelationId)
    }

    private fun getRequestCorrelationId(exchange: ServerWebExchange): String? {
        return exchange.request.headers.getFirst(CORRELATION_ID_HEADER_NAME)
    }
}
