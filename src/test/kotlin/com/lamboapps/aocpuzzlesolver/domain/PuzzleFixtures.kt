package com.lamboapps.aocpuzzlesolver.domain

object PuzzleFixtures {

    val DEFAULT_PUZZLE = Puzzle("2019-day01", "https://example.com/2019/day/1", PuzzleInputFormat.COMMA_SEPARATED)
    const val DEFAULT_SOLVER_SCRIPT = "fun main() = println(\"hello world\")"
    const val DEFAULT_PUZZLE_INPUT = "0123,456,789"
}
