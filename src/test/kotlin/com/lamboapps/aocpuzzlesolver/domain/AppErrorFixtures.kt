package com.lamboapps.aocpuzzlesolver.domain

object AppErrorFixtures {

    val DEFAULT_APP_ERROR = InternalAppError("Generic error", RuntimeException("Generic cause"), listOf("details"))
}
