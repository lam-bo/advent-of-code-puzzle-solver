package com.lamboapps.aocpuzzlesolver.driven.solution

import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.driven.solution.ScriptEngineSolutionRepository.Companion.SOLVER_SCRIPT_EVALUATION_ERROR
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import javax.script.ScriptException

internal class ScriptEngineSolutionRepositoryTest {

    private val repository = ScriptEngineSolutionRepository()

    @Test
    fun `should create solution`(): Unit = runBlocking {
        // given
        val puzzleInput = "puzzleInput"
        val solverScript = "{ input: String -> input }"

        // when
        val actualResult = repository.createSolution(puzzleInput, solverScript)

        // then
        assertThat(actualResult.orNull()).isEqualTo(puzzleInput)
    }

    @Test
    fun `create solution should return internal error when script is not kotlin code`(): Unit = runBlocking {
        // given
        val puzzleInput = "puzzleInput"
        val solverScript = "not kotlin code"

        // when
        val actualError = repository.createSolution(puzzleInput, solverScript).swap().orNull()!!

        // then
        assertThat(actualError).isInstanceOf(InternalAppError::class.java)
        assertThat(actualError.throwable).isInstanceOf(ScriptException::class.java)
        assertThat(actualError.message).isEqualTo(SOLVER_SCRIPT_EVALUATION_ERROR)
    }

    @Test
    fun `create solution should return internal error when evaluated script is not a kotlin lambda`(): Unit =
        runBlocking {
            // given
            val puzzleInput = "puzzleInput"
            val solverScript = "2 + 3"

            // when
            val actualError = repository.createSolution(puzzleInput, solverScript).swap().orNull()!!

            // then
            assertThat(actualError).isInstanceOf(InternalAppError::class.java)
            assertThat(actualError.throwable).isInstanceOf(ClassCastException::class.java)
            assertThat(actualError.message).isEqualTo(SOLVER_SCRIPT_EVALUATION_ERROR)
        }

    @Test
    fun `create solution should return internal error when script invocation throws exception`(): Unit = runBlocking {
        // given
        val puzzleInput = "puzzleInput"
        val solverScript = "{ input: String -> throw RuntimeException(); input }"

        // when
        val actualError = repository.createSolution(puzzleInput, solverScript).swap().orNull()!!

        // then
        assertThat(actualError).isInstanceOf(InternalAppError::class.java)
        assertThat(actualError.throwable).isInstanceOf(RuntimeException::class.java)
        assertThat(actualError.message).isEqualTo(SOLVER_SCRIPT_EVALUATION_ERROR)
    }
}
