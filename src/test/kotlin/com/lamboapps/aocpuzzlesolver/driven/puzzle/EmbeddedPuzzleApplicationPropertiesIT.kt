package com.lamboapps.aocpuzzlesolver.driven.puzzle

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("test")
internal class EmbeddedPuzzleApplicationPropertiesIT(@Autowired val properties: EmbeddedPuzzleApplicationProperties) {

    @Test
    fun `should get embedded puzzles`() {
        // given
        // when
        val actualResult = properties.embeddedPuzzles

        // then
        val expectedFirstPuzzle = EmbeddedPuzzle(
            "2019-day01", "https://example.com/2019/day/1",
            EmbeddedPuzzleInputFormat.MULTI_LINE, "aoc-scripts/2019-day01-part1.kts"
        )
        val expectedSecondPuzzle = EmbeddedPuzzle(
            "2019-day02", "https://example.com/2019/day/2",
            EmbeddedPuzzleInputFormat.COMMA_SEPARATED, "aoc-scripts/2019-day02-part1.kts"
        )
        val expectedThirdPuzzle = EmbeddedPuzzle(
            "2019-day03", "https://example.com/2019/day/3",
            EmbeddedPuzzleInputFormat.COMMA_SEPARATED, "not-an-existing-path"
        )

        assertThat(actualResult[0]).isEqualTo(expectedFirstPuzzle)
        assertThat(actualResult[1]).isEqualTo(expectedSecondPuzzle)
        assertThat(actualResult[2]).isEqualTo(expectedThirdPuzzle)
    }
}
