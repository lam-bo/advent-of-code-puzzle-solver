package com.lamboapps.aocpuzzlesolver.driven.puzzle

import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.domain.NotFoundError
import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleInputFormat
import com.lamboapps.aocpuzzlesolver.domain.PuzzleRepository
import com.lamboapps.aocpuzzlesolver.driven.puzzle.EmbeddedPuzzleRepository.Companion.NO_PUZZLE_FOUND_MESSAGE
import com.lamboapps.aocpuzzlesolver.driven.puzzle.EmbeddedPuzzleRepository.Companion.SOLVER_SCRIPT_RETRIEVAL_ERROR
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.io.File
import java.io.FileNotFoundException

@SpringBootTest
@ActiveProfiles("test")
internal class EmbeddedPuzzleRepositoryIT(@Autowired val puzzleRepository: PuzzleRepository) {
    companion object Fixtures {
        val FIRST_EMBEDDED_PUZZLE = EmbeddedPuzzle(
            "2019-day01",
            "https://example.com/2019/day/1",
            EmbeddedPuzzleInputFormat.MULTI_LINE,
            "aoc-scripts/2019-day01-part1.kts"
        )

        val FIRST_SOLVER_SCRIPT = File(FIRST_EMBEDDED_PUZZLE.solverPath).readText()

        private val SECOND_EMBEDDED_PUZZLE = EmbeddedPuzzle(
            "2019-day02",
            "https://example.com/2019/day/2",
            EmbeddedPuzzleInputFormat.COMMA_SEPARATED,
            "classpath:aoc-scripts/2019-day02-part1.kts"
        )
        private val THIRD_EMBEDDED_PUZZLE = EmbeddedPuzzle(
            "2019-day03",
            "https://example.com/2019/day/3",
            EmbeddedPuzzleInputFormat.COMMA_SEPARATED,
            "not-an-existing-path"
        )
        private val EMBEDDED_PUZZLES = setOf(FIRST_EMBEDDED_PUZZLE, SECOND_EMBEDDED_PUZZLE, THIRD_EMBEDDED_PUZZLE)
        val PUZZLES = EMBEDDED_PUZZLES.map { it.toDomain() }.toSet()
    }

    @Test
    fun `should get puzzles`(): Unit = runBlocking {
        // given
        // when
        val actualPuzzles = puzzleRepository.getPuzzles().orNull()

        // then
        assertThat(actualPuzzles).isEqualTo(PUZZLES)
    }

    @Test
    fun `should get puzzle`(): Unit = runBlocking {
        // given
        val expectedPuzzle = FIRST_EMBEDDED_PUZZLE.toDomain()

        // when
        val actualPuzzle = puzzleRepository.getPuzzle(expectedPuzzle.id).orNull()

        // then
        assertThat(actualPuzzle).isEqualTo(expectedPuzzle)
    }

    @Test
    fun `get puzzle should return not found error when puzzle doesn't exist`(): Unit = runBlocking {
        // given
        // when
        val actualError = puzzleRepository.getPuzzle("random").swap().orNull()

        // then
        assertThat(actualError).isEqualTo(NotFoundError(NO_PUZZLE_FOUND_MESSAGE))
    }

    @Test
    fun `should get solver script`(): Unit = runBlocking {
        // given
        val puzzle = FIRST_EMBEDDED_PUZZLE.toDomain()

        // when
        val actualSolverScript = puzzleRepository.getSolverScript(puzzle).orNull()

        // then
        assertThat(actualSolverScript).isEqualTo(FIRST_SOLVER_SCRIPT)
    }

    @Test
    fun `get solver script should return internal error when puzzle doesn't exist`(): Unit = runBlocking {
        // given
        val puzzle = Puzzle("random", "random", PuzzleInputFormat.COMMA_SEPARATED)

        // when
        val actualError = puzzleRepository.getSolverScript(puzzle).swap().orNull()!!

        // then
        assertThat(actualError).isInstanceOf(InternalAppError::class.java)
        assertThat(actualError.message).isEqualTo(SOLVER_SCRIPT_RETRIEVAL_ERROR)
        assertThat(actualError.throwable).isInstanceOf(NullPointerException::class.java)
    }

    @Test
    fun `get solver script should return internal error when script file doesn't exist`(): Unit = runBlocking {
        // given
        val puzzle = THIRD_EMBEDDED_PUZZLE.toDomain()

        // when
        val actualError = puzzleRepository.getSolverScript(puzzle).swap().orNull()!!

        // then
        assertThat(actualError).isInstanceOf(InternalAppError::class.java)
        assertThat(actualError.message).isEqualTo(SOLVER_SCRIPT_RETRIEVAL_ERROR)
        assertThat(actualError.throwable).isInstanceOf(FileNotFoundException::class.java)
    }
}
