package com.lamboapps.aocpuzzlesolver.driving.api.puzzle

import arrow.core.left
import arrow.core.right
import com.lamboapps.aocpuzzlesolver.domain.AppErrorFixtures.DEFAULT_APP_ERROR
import com.lamboapps.aocpuzzlesolver.domain.PuzzleFixtures.DEFAULT_PUZZLE
import com.lamboapps.aocpuzzlesolver.domain.PuzzleFixtures.DEFAULT_PUZZLE_INPUT
import com.lamboapps.aocpuzzlesolver.domain.PuzzleFixtures.DEFAULT_SOLVER_SCRIPT
import com.lamboapps.aocpuzzlesolver.domain.PuzzleRepository
import com.lamboapps.aocpuzzlesolver.domain.SolutionFixtures.DEFAULT_SOLUTION
import com.lamboapps.aocpuzzlesolver.domain.SolutionRepository
import com.lamboapps.aocpuzzlesolver.driving.api.common.ApiCommonFixtures.DEFAULT_CORRELATION_ID
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationRecorder
import com.lamboapps.aocpuzzlesolver.driving.api.common.ResponseEntityAdapter
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.unmockkObject
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

internal class PuzzleControllerTest {
    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeAll() = mockkObject(PuzzleValidation)

        @AfterAll
        @JvmStatic
        fun afterAll() = unmockkObject(PuzzleValidation)
    }

    private val puzzleRepository: PuzzleRepository = mockk()
    private val solutionRepository: SolutionRepository = mockk()
    private val operationRecorder: OperationRecorder = mockk()
    private val puzzleController: PuzzleController =
        PuzzleController(puzzleRepository, solutionRepository, operationRecorder)

    init {
        every { operationRecorder.recordOperationFailure(any(), any()) } returns Unit
        every { operationRecorder.recordOperationSuccess(any()) } returns Unit
    }

    @Test
    fun `should get puzzles`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzles() } returns setOf(DEFAULT_PUZZLE).right()

        // when
        val actualResult = puzzleController.getPuzzles(DEFAULT_CORRELATION_ID)

        // then
        val expectedBody = listOf(PuzzleModel.fromDomain(DEFAULT_PUZZLE))
        val expectedResponse = ResponseEntityAdapter.fromResponse(HttpStatus.OK, expectedBody)

        verify { operationRecorder.recordOperationSuccess(any()) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `get puzzles should return error when repository get puzzles returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzles() } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult = puzzleController.getPuzzles(DEFAULT_CORRELATION_ID)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `should get puzzle`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()

        // when
        val actualResult = puzzleController.getPuzzle(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id)

        // then
        val expectedBody = PuzzleModel.fromDomain(DEFAULT_PUZZLE)
        val expectedResponse = ResponseEntityAdapter.fromResponse(HttpStatus.OK, expectedBody)

        verify { operationRecorder.recordOperationSuccess(any()) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `get puzzle should return error when repository get puzzle returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult = puzzleController.getPuzzle(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `should get puzzle solver script`() = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
        coEvery { puzzleRepository.getSolverScript(DEFAULT_PUZZLE) } returns DEFAULT_SOLVER_SCRIPT.right()

        // when
        val actualResult = puzzleController.getPuzzleSolverScript(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id)

        // then
        val expectedResponse =
            ResponseEntityAdapter.fromResponse(HttpStatus.OK, DEFAULT_SOLVER_SCRIPT, MediaType.TEXT_PLAIN)

        assertThat(actualResult).isEqualTo(expectedResponse)
        verify { operationRecorder.recordOperationSuccess(any()) }
    }

    @Test
    fun `get puzzle solver script should return error when repository get puzzle returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult = puzzleController.getPuzzleSolverScript(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `get puzzle solver script should return error when repository get solver script returns error`(): Unit =
        runBlocking {
            // given
            coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
            coEvery { puzzleRepository.getSolverScript(DEFAULT_PUZZLE) } returns DEFAULT_APP_ERROR.left()

            // when
            val actualResult = puzzleController.getPuzzleSolverScript(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id)

            // then
            val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

            verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
            assertThat(actualResult).isEqualTo(expectedResponse)
        }

    @Test
    fun `should create solution`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
        every { PuzzleValidation.validatePuzzleInput(DEFAULT_PUZZLE_INPUT, DEFAULT_PUZZLE) } returns null
        coEvery { puzzleRepository.getSolverScript(DEFAULT_PUZZLE) } returns DEFAULT_SOLVER_SCRIPT.right()
        coEvery {
            solutionRepository.createSolution(
                DEFAULT_PUZZLE_INPUT,
                DEFAULT_SOLVER_SCRIPT
            )
        } returns DEFAULT_SOLUTION.right()

        // when
        val actualResult =
            puzzleController.createPuzzleSolution(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id, DEFAULT_PUZZLE_INPUT)

        // then
        val expectedResponse =
            ResponseEntityAdapter.fromResponse(HttpStatus.OK, DEFAULT_SOLUTION, MediaType.TEXT_PLAIN)

        verify { operationRecorder.recordOperationSuccess(any()) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `create solution should return error when repository get puzzle returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult =
            puzzleController.createPuzzleSolution(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id, DEFAULT_PUZZLE_INPUT)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `create solution should return error when puzzle input validation returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
        every { PuzzleValidation.validatePuzzleInput(DEFAULT_PUZZLE_INPUT, DEFAULT_PUZZLE) } returns DEFAULT_APP_ERROR

        // when
        val actualResult =
            puzzleController.createPuzzleSolution(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id, DEFAULT_PUZZLE_INPUT)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `create solution should return error when repository get solver script returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
        every { PuzzleValidation.validatePuzzleInput(DEFAULT_PUZZLE_INPUT, DEFAULT_PUZZLE) } returns null
        coEvery { puzzleRepository.getSolverScript(DEFAULT_PUZZLE) } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult =
            puzzleController.createPuzzleSolution(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id, DEFAULT_PUZZLE_INPUT)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }

    @Test
    fun `create solution should return error when repository create solution returns error`(): Unit = runBlocking {
        // given
        coEvery { puzzleRepository.getPuzzle(DEFAULT_PUZZLE.id) } returns DEFAULT_PUZZLE.right()
        every { PuzzleValidation.validatePuzzleInput(DEFAULT_PUZZLE_INPUT, DEFAULT_PUZZLE) } returns null
        coEvery { puzzleRepository.getSolverScript(DEFAULT_PUZZLE) } returns DEFAULT_SOLVER_SCRIPT.right()
        coEvery {
            solutionRepository.createSolution(
                DEFAULT_PUZZLE_INPUT,
                DEFAULT_SOLVER_SCRIPT
            )
        } returns DEFAULT_APP_ERROR.left()

        // when
        val actualResult =
            puzzleController.createPuzzleSolution(DEFAULT_CORRELATION_ID, DEFAULT_PUZZLE.id, DEFAULT_PUZZLE_INPUT)

        // then
        val expectedResponse = ResponseEntityAdapter.fromAppError(DEFAULT_APP_ERROR)

        verify { operationRecorder.recordOperationFailure(any(), eq(DEFAULT_APP_ERROR)) }
        assertThat(actualResult).isEqualTo(expectedResponse)
    }
}
