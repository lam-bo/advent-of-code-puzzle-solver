package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.driving.api.common.ApiCommonFixtures.DEFAULT_CORRELATION_ID
import com.lamboapps.aocpuzzlesolver.driving.api.common.CorrelationIdHeaderFilter.Companion.CORRELATION_ID_HEADER_NAME
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import reactor.kotlin.test.test

internal class CorrelationIdHeaderFilterTest {

    private val correlationIdProvider: () -> String = mockk()
    private val exchange: ServerWebExchange = mockk(relaxed = true)
    private val chain: WebFilterChain = mockk(relaxed = true)
    private val correlationIdHeaderFilter = CorrelationIdHeaderFilter(correlationIdProvider)

    @Test
    fun `filter should copy request correlation id header to response`() {
        // given
        every { exchange.request.headers.getFirst(CORRELATION_ID_HEADER_NAME) } returns DEFAULT_CORRELATION_ID
        every { chain.filter(exchange) } returns Mono.just(true).then()

        // when
        val actualResultMono = correlationIdHeaderFilter.filter(exchange, chain)

        // then
        actualResultMono.test().verifyComplete()
        verify { exchange.response.headers.set(CORRELATION_ID_HEADER_NAME, DEFAULT_CORRELATION_ID) }
        verify { chain.filter(exchange) }
    }

    @Test
    fun `filter should generate correlation id header for request if not provided`() {
        // given
        val generatedCorrelationId = "generatedCorrelationId"
        val mutatedRequest: ServerHttpRequest = mockk(relaxed = true)
        val exchangeWithCorrelationId: ServerWebExchange = mockk(relaxed = true)

        every { correlationIdProvider.invoke() } returns generatedCorrelationId
        every { exchange.request.headers.getFirst(CORRELATION_ID_HEADER_NAME) } returns null
        every {
            exchange.request.mutate().header(CORRELATION_ID_HEADER_NAME, generatedCorrelationId).build()
        } returns mutatedRequest
        every { exchange.mutate().request(mutatedRequest).build() } returns exchangeWithCorrelationId
        every { chain.filter(exchangeWithCorrelationId) } returns Mono.just(true).then()

        // when
        val actualResultMono = correlationIdHeaderFilter.filter(exchange, chain)

        // then
        actualResultMono.test().verifyComplete()
        verify { exchangeWithCorrelationId.response.headers.set(CORRELATION_ID_HEADER_NAME, generatedCorrelationId) }
        verify { chain.filter(exchangeWithCorrelationId) }
    }
}
