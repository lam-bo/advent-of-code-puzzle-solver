package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.domain.InternalAppError
import com.lamboapps.aocpuzzlesolver.domain.NotFoundError
import com.lamboapps.aocpuzzlesolver.domain.NotImplementedError
import com.lamboapps.aocpuzzlesolver.domain.ValidationError
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

internal class ResponseEntityAdapterTest {

    @TestFactory
    fun testFromAppError() = listOf(
        NotFoundError("message", null, listOf("details")) to HttpStatus.NOT_FOUND,
        NotImplementedError("message", null, listOf("details")) to HttpStatus.NOT_IMPLEMENTED,
        ValidationError("message", null, listOf("details")) to HttpStatus.BAD_REQUEST,
        InternalAppError("message", null, listOf("details")) to HttpStatus.INTERNAL_SERVER_ERROR
    )
        .map { (appError, expectedStatus) ->
            dynamicTest("should adapt from app error ${appError.javaClass.simpleName}") {
                // given
                // when
                val actualResult = ResponseEntityAdapter.fromAppError(appError)

                // then
                assertThat(actualResult.statusCode).isEqualTo(expectedStatus)
                assertThat(actualResult.headers.contentType).isEqualTo(MediaType.APPLICATION_JSON)
                assertThat(actualResult.body).isEqualTo(AppErrorModel.fromDomain(appError))
            }
        }

    @Test
    fun `adapt from response should put content type when body is not empty`() {
        // given
        val responseBody = "responseBody"
        val status = HttpStatus.OK
        val contentType = MediaType.TEXT_PLAIN

        // when
        val actualResult = ResponseEntityAdapter.fromResponse(status, responseBody, contentType)

        // then
        assertThat(actualResult.body).isEqualTo(responseBody)
        assertThat(actualResult.statusCode).isEqualTo(status)
        assertThat(actualResult.headers.contentType).isEqualTo(contentType)
    }

    @Test
    fun `adapt from response should ignore content type when body is empty`() {
        // given
        val status = HttpStatus.OK

        // when
        val actualResult = ResponseEntityAdapter.fromResponse(status)

        // then
        assertThat(actualResult.body).isNull()
        assertThat(actualResult.statusCode).isEqualTo(status)
        assertThat(actualResult.headers.contentType).isNull()
    }
}
