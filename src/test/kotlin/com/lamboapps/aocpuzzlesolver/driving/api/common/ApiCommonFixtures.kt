package com.lamboapps.aocpuzzlesolver.driving.api.common

object ApiCommonFixtures {

    const val DEFAULT_CORRELATION_ID = "correlationId"
    const val DEFAULT_OPERATION_ID = "operationId"
    val DEFAULT_OPERATION_CONTEXT = OperationContext.Builder()
        .put("key1", "value1")
        .put("key2", "value2")
        .build(DEFAULT_OPERATION_ID, DEFAULT_CORRELATION_ID)
}
