package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.lamboapps.aocpuzzlesolver.domain.AppErrorFixtures.DEFAULT_APP_ERROR
import com.lamboapps.aocpuzzlesolver.driving.api.common.ApiCommonFixtures.DEFAULT_CORRELATION_ID
import com.lamboapps.aocpuzzlesolver.driving.api.common.ApiCommonFixtures.DEFAULT_OPERATION_ID
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationContext.Builder.Companion.APP_ERROR_DETAILS_KEY
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationContext.Builder.Companion.APP_ERROR_MESSAGE_KEY
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationContext.Builder.Companion.CORRELATION_ID_KEY
import com.lamboapps.aocpuzzlesolver.driving.api.common.OperationContext.Builder.Companion.OPERATION_ID_KEY
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class OperationContextTest {

    @Test
    fun `should build operation context`() {
        // given
        val builder = OperationContext.Builder()
            .put("key1", "value1")
            .putNullable("key2", null)
            .putNullable("key3", "value3")
            .putTry("key4", Result.success("value4"))
            .putTry("key5", Result.failure(RuntimeException()))
            .putAppError(DEFAULT_APP_ERROR)

        // when
        val actualResult = builder.build(DEFAULT_OPERATION_ID, DEFAULT_CORRELATION_ID)

        // then
        val expectedLogEntries = mapOf(
            "key1" to "value1",
            "key3" to "value3",
            "key4" to "value4",
            APP_ERROR_MESSAGE_KEY to DEFAULT_APP_ERROR.message,
            APP_ERROR_DETAILS_KEY to DEFAULT_APP_ERROR.details.joinToString("\n"),
            CORRELATION_ID_KEY to DEFAULT_CORRELATION_ID,
            OPERATION_ID_KEY to DEFAULT_OPERATION_ID
        )
        assertThat(actualResult.logEntries).isEqualTo(expectedLogEntries)
    }
}
