package com.lamboapps.aocpuzzlesolver.driving.api.puzzle

import com.lamboapps.aocpuzzlesolver.domain.Puzzle
import com.lamboapps.aocpuzzlesolver.domain.PuzzleInputFormat
import com.lamboapps.aocpuzzlesolver.domain.ValidationError
import com.lamboapps.aocpuzzlesolver.driving.api.puzzle.PuzzleValidation.PUZZLE_INPUT_FORMAT_VALIDATION_ERROR
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class PuzzleValidationTest {

    @Test
    fun `should validate puzzle input when input is comma separated`() {
        // given
        val puzzle = Puzzle("2019-day01", "https://example.com/2019/day/1", PuzzleInputFormat.COMMA_SEPARATED)
        val puzzleInput = "0123,456,789"

        // when
        val actualError = PuzzleValidation.validatePuzzleInput(puzzleInput, puzzle)

        // then
        assertThat(actualError).isNull()
    }

    @Test
    fun `should validate puzzle input when input is multi line`() {
        // given
        val puzzle = Puzzle("2019-day01", "https://example.com/2019/day/1", PuzzleInputFormat.MULTI_LINE)
        val puzzleInput = "0123\n456\n789"

        // when
        val actualError = PuzzleValidation.validatePuzzleInput(puzzleInput, puzzle)

        // then
        assertThat(actualError).isNull()
    }

    @Test
    fun `validate puzzle input should return validation error when comma separated input is not valid`() {
        // given
        val puzzle = Puzzle("2019-day01", "https://example.com/2019/day/1", PuzzleInputFormat.COMMA_SEPARATED)
        val puzzleInput = "0123\n456\n789"

        // when
        val actualError = PuzzleValidation.validatePuzzleInput(puzzleInput, puzzle)

        // then
        assertThat(actualError).isInstanceOf(ValidationError::class.java)
        assertThat(actualError!!.message).isEqualTo(PUZZLE_INPUT_FORMAT_VALIDATION_ERROR)
    }

    @Test
    fun `validate puzzle input should return validation error when multi line input is not valid`() {
        // given
        val puzzle = Puzzle("2019-day01", "https://example.com/2019/day/1", PuzzleInputFormat.MULTI_LINE)
        val puzzleInput = "0123,456,789"

        // when
        val actualError = PuzzleValidation.validatePuzzleInput(puzzleInput, puzzle)

        // then
        assertThat(actualError).isInstanceOf(ValidationError::class.java)
        assertThat(actualError!!.message).isEqualTo(PUZZLE_INPUT_FORMAT_VALIDATION_ERROR)
    }
}
