package com.lamboapps.aocpuzzlesolver.driving.api.monitor

import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class PingControllerTest {

    private val pingController = PingController()

    @Test
    fun `should return pong response`(): Unit = runBlocking {
        // given
        // when
        val actualResult = pingController.pong()

        // then
        assertThat(actualResult).isEqualTo("PONG")
    }
}
