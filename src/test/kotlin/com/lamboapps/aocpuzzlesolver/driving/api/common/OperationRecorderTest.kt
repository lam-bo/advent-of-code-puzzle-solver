package com.lamboapps.aocpuzzlesolver.driving.api.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.lamboapps.aocpuzzlesolver.domain.AppErrorFixtures.DEFAULT_APP_ERROR
import com.lamboapps.aocpuzzlesolver.driving.api.common.ApiCommonFixtures.DEFAULT_OPERATION_CONTEXT
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.slf4j.Logger

internal class OperationRecorderTest {

    private val objectMapper = ObjectMapper()
    private val logger: Logger = mockk(relaxUnitFun = true)
    private val operationRecorder = OperationRecorder(logger)

    @Test
    fun `should record operation success`() {
        // given
        // when
        operationRecorder.recordOperationSuccess(DEFAULT_OPERATION_CONTEXT)

        // then
        val expectedSerializedEntries = objectMapper.writeValueAsString(DEFAULT_OPERATION_CONTEXT.logEntries)
        verify { logger.info(expectedSerializedEntries) }
    }

    @Test
    fun `should record operation failure`() {
        // given
        // when
        operationRecorder.recordOperationFailure(DEFAULT_OPERATION_CONTEXT, DEFAULT_APP_ERROR)

        // then
        val expectedSerializedEntries = objectMapper.writeValueAsString(DEFAULT_OPERATION_CONTEXT.logEntries)
        verify { logger.error(expectedSerializedEntries, DEFAULT_APP_ERROR.throwable) }
    }
}
